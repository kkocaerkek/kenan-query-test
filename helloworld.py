from google.appengine.ext import webapp
from google.appengine.ext.webapp.util import run_wsgi_app

from google.appengine.ext import db
import random
from datetime import datetime


class MainPage(webapp.RequestHandler):
    
    
    def get(self):
        self.response.headers['Content-Type'] = 'text/plain'
        self.response.out.write('Hello, webapp World!')


categories = ["cat1", "cat2", "cat3"]
class SimpleModel(db.Model):
    name = db.StringProperty()
    date_value = db.DateTimeProperty()
    category = db.StringProperty(choices=categories)
    


class InitHandler(webapp.RequestHandler):
    def get(self):
        entity = SimpleModel.all().get()
        if entity is None:
            for i in range(1, 1000):
                entity = SimpleModel()
                entity.name = "Entity %s" % i
                entity.category = random.choice(categories)
                entity.date_value = random.choice([None, datetime.now()])
                entity.put()
        self.response.headers['Content-Type'] = 'text/plain'
        self.response.out.write('Sample data initialized!')


class SimpleQueryHandler(webapp.RequestHandler):
    def get(self):
        query = SimpleModel.all().filter("category", "cat1").order("date_value")
        self.response.headers['Content-Type'] = 'text/plain'
        for entity in query:
            self.response.out.write('%s\n' % entity.name)
            
class InQueryHandler(webapp.RequestHandler):
    def get(self):
        query = SimpleModel.all().filter("category IN", ["cat1", "cat2"]).order("date_value")
        self.response.headers['Content-Type'] = 'text/plain'
        for entity in query:
            self.response.out.write('%s\n' % entity.name)


application = webapp.WSGIApplication(
    [
        ('/', MainPage),
        ('/init-data', InitHandler),
        ('/simple-query', SimpleQueryHandler),
        ('/in-query', InQueryHandler)
        
    ], debug=True)


def main():
    run_wsgi_app(application)

if __name__ == "__main__":
    main()
